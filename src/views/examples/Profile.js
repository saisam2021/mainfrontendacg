/*!

=========================================================
* Argon Dashboard React - v1.2.0
=========================================================

* Product Page: https://www.creative-tim.com/product/argon-dashboard-react
* Copyright 2021 Creative Tim (https://www.creative-tim.com)
* Licensed under MIT (https://github.com/creativetimofficial/argon-dashboard-react/blob/master/LICENSE.md)

* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/
import React from "react";

// reactstrap components
import {
  Button,
  Card,
  CardHeader,
  CardBody,
  FormGroup,
  Form,
  Input,
  Container,
  Row,
  Col,
} from "reactstrap";
// core components
import UserHeader from "components/Headers/UserHeader.js";

const Profile = () => {
  return (
    <>
      <UserHeader />
      {/* Page content */}
      <Container className="mt--7" fluid>
        <Row>
          <Col className="order-xl-2 mb-5 mb-xl-0" xl="4">
            <Card className="card-profile shadow cardprofile">
              <Row className="justify-content-center">
                <Col className="order-lg-2" lg="3">
                  <div className="card-profile-image">
                    <a href="#pablo" onClick={(e) => e.preventDefault()}>
                      <img
                        alt="..."
                        className="rounded-circle"
                        src={
                          require("../../assets/img/theme/sai.png")
                            .default
                        }
                      />
                    </a>
                  </div>
                </Col>
              </Row>
              <CardHeader className="text-center border-0 pt-8 pt-md-4 pb-0 pb-md-4">
                <div className="d-flex justify-content-between">
                  
                </div>
              </CardHeader>
              <CardBody className="pt-0 pt-md-4">
                <Row>
                  <div className="col">
                    <div className="card-profile-stats d-flex justify-content-center mt-md-5">
                    </div>
                  </div>
                </Row>
                <div className="text-center">
                  <h3>
                    Sai Sur
                  </h3>
                  <div className="h5 font-weight-300">
                    <i className="ni location_pin mr-2" />
                    Ann Arbor, Michigan
                  </div>
                  <div className="h5 mt-4">
                    <i className="ni business_briefcase-24 mr-2" />
                    Data Science and Full Stack 
                  </div>
                  <div>
                    <i className="ni education_hat mr-2" />
                    University of Michigan
                  </div>
                  <hr className="my-4" />
                  <p>
                    Senior at the School of Information. Love working hard, intrested in NLP and Data Science. Fishing and Geography enthusiast. Will be going to grad school at Columbia next.
                  </p>
                </div>
              </CardBody>
            </Card>
            <Card className="card-profile shadow cardprofile" >
              <Row className="justify-content-center">
                <Col className="order-lg-2" lg="3">
                  <div className="card-profile-image">
                    <a href="#pablo" onClick={(e) => e.preventDefault()}>
                      <img
                        alt="..."
                        className="rounded-circle"
                        src={
                          require("../../assets/img/theme/miles.jpeg")
                            .default
                        }
                      />
                    </a>
                  </div>
                </Col>
              </Row>
              <CardHeader className="text-center border-0 pt-8 pt-md-4 pb-0 pb-md-4">
                <div className="d-flex justify-content-between">
                  
                </div>
              </CardHeader>
              <CardBody className="pt-0 pt-md-4">
                <Row>
                  <div className="col">
                    <div className="card-profile-stats d-flex justify-content-center mt-md-5">
                    </div>
                  </div>
                </Row>
                <div className="text-center">
                  <h3>
                    Miles Butler
                  </h3>
                  <div className="h5 font-weight-300">
                    <i className="ni location_pin mr-2" />
                    Ann Arbor, Michigan
                  </div>
                  <div className="h5 mt-4">
                    <i className="ni business_briefcase-24 mr-2" />
                    Data Science
                  </div>
                  <div>
                    <i className="ni education_hat mr-2" />
                    University of Michigan
                  </div>
                  <hr className="my-4" />
                  <p>
                  Senior at the School of Information. Intrested in Machine learning. Cross Country runner. Going to Harvard for grad school next.
                  </p>
                </div>
              </CardBody>
            </Card>
            <Card className="card-profile shadow" >
              <Row className="justify-content-center">
                <Col className="order-lg-2" lg="3">
                  <div className="card-profile-image">
                    <a href="#pablo" onClick={(e) => e.preventDefault()}>
                      <img
                        alt="..."
                        className="rounded-circle"
                        src={
                          require("../../assets/img/theme/emily.jpeg")
                            .default
                        }
                      />
                    </a>
                  </div>
                </Col>
              </Row>
              <CardHeader className="text-center border-0 pt-8 pt-md-4 pb-0 pb-md-4">
                <div className="d-flex justify-content-between">
                  
                </div>
              </CardHeader>
              <CardBody className="pt-0 pt-md-4">
                <Row>
                  <div className="col">
                    <div className="card-profile-stats d-flex justify-content-center mt-md-5">
                    </div>
                  </div>
                </Row>
                <div className="text-center">
                  <h3>
                    Emily Lin 
                  </h3>
                  <div className="h5 font-weight-300">
                    <i className="ni location_pin mr-2" />
                    Ann Arbor, Michigan
                  </div>
                  <div className="h5 mt-4">
                    <i className="ni business_briefcase-24 mr-2" />
                    Data Science
                  </div>
                  <div>
                    <i className="ni education_hat mr-2" />
                    University of Michigan
                  </div>
                  <hr className="my-4" />
                  <p>
                  Senior at the School of Information. Likes board games, traveling. Joining the Expedia Data Team next.
                  </p>
                </div>
              </CardBody>
            </Card>
          </Col>
          <Col className="order-xl-1" xl="8">
            <Card className="bg-secondary shadow">
              <CardHeader className="bg-white border-0">
                <Row className="align-items-center">
                  <Col xs="8">
                    <h3 className="mb-0">Vajra: Anomoly Detection and Time Series</h3>
                  </Col>
                </Row>
              </CardHeader>
              <CardBody>
              <div className="pl-lg-4">
                  <h4 className="heading-small text-muted mb-4">
                  Problem Statement
                  </h4>
                  
                  <p>The main objective of this project is that ACG would like to develop a system that can predict future breakdowns on their members’ vehicles. Understanding and predicting when and where vehicle breakdowns will likely occur will help ACG provide timely services and products through proper staffing and stocking of products. With the help of this predictive model, ACG can staff an efficient number of roadside assistance delivery teams at individual service centers, and stock products such as dead battery or flat tire replacements more accurately. These actions will increase sales performance and also improve the members’ experience by delivering timely roadside assistance.</p>
                  <p>The secondary goal of this project is to detect potentially fraudulent behavior/misuse of AAA’s services through an ACG membership. ACG would like to understand how and when users are potentially misusing AAA’S services. An example of such misuse is when used car dealerships register for an individual ACG membership, and use it to tow different vehicles to their lot to save towing fees. Identifying when this type of behavior occurs, and flagging individuals that are likely misusing ACG’s services will help the organization with fraud detection and save roadside assistance teams a lot of effort on delivering unnecessary services.</p>

                  
                  {<hr className="my-4" />}
                  <h4 className="heading-small text-muted mb-4">About me</h4>

                  <p>The minimum viable product (MVP) of our project is to create a predictive model that can help predict  future vehicle breakdowns and demand of individual AAA service centers over time.</p>
              <p>The reach objective of our project is to create an anomaly detection model that can help identify individuals misusing AAA’s services. Our responsibilities are to conduct data analysis, create machine learning prototypes, and present to the business in an iterative manner until the solution is delivering the outcomes desired by the roadside assistance teams.</p>
      {<hr className="my-4" />}
      <h4 className="heading-small text-muted mb-4">Value Added</h4>
      <p>Our team’s minimum viable product (MVP) for this project is to create a predictive model that can forecast demand of vehicle breakdowns on the individual service center level. This model will be beneficial for ACG because it will provide an understanding of where there exists staffing or product stocking inefficiencies, and what ACG can do to tackle these problems. ACG can also compare the prediction results of our model with existing demand models within the organization to improve the accuracy of predictions.</p>
      <p>Our anomaly detection model can help identify individuals likely misusing AAA’s services through ACG. This model will be beneficial for cross-functional teams within ACG, such as the roadside assistance team, risk assessment and fraud detection team, and actuaries. Identifying individuals likely misusing AAA’s services narrows down the scope of investigation, prevents the waste of the roadside assistance team’s time and efforts delivering services where they should not be, and ensures the proper use of ACG’s services and resources by members. ACG can also use our model to assess current fraud detection measures within the organization and cross-compare results to identify patterns that may have been missed.</p>
  
                  <div className="pl-lg-4"></div> 
                  </div>
              </CardBody>
            </Card>
          </Col>
        </Row>
      </Container>
    </>
  );
};

export default Profile;
