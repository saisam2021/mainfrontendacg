
import React, { useState } from "react";
// node.js library that concatenates classes (strings)
import classnames from "classnames";
// javascipt plugin for creating charts
import Chart from "chart.js";
// react plugin used to create charts
import { Line, Bar } from "react-chartjs-2";
// reactstrap components
import {
  Button,
  Card,
  CardHeader,
  CardBody,
  NavItem,
  NavLink,
  Nav,
  Progress,
  Table,
  Container,
  Row,
  Col,
} from "reactstrap";

// core components
import {
  chartOptions,
  parseOptions,
  chartExample1,
  // chartExample2,
  chartExample3,
  
} from "variables/charts.js";

import Header from "components/Headers/AnomolyHeader.js";

import ReactDOM from "react-dom";
import Graph from "react-graph-vis";

const graph = {
  nodes: [{id:1,'label': "31750", 'title': "{'nodetype': 'zip'}"},
 {id:2,'label': "31769", 'title': "{'nodetype': 'zip'}"},
 {id:3,'label': "61745407",'title': "{'nodetype': 'breakdown', 'time_called': '02/11/2016, 10:57:00', 'billed': 155.0, 'tihme_arrived': '02/11/2016, 12:57:00'}"},
 {id:4,'label': "58848539", 'title': "{'nodetype': 'breakdown', 'time_called': '07/27/2015, 09:08:00', 'billed': 0.0, 'tihme_arrived': '07/27/2015, 11:12:00'}"},
 {id:5,'label': 'id_3015048086.0', 'title': 'car'},
 {id:6,'label': 'id_3021433465.0', 'title': 'car'},
 {id:7,'label': 'YUKON_2002', 'title': 'car'},
 {id:8,'label': 'TRAILBLAZER_2007', 'title': 'car'},
         {id:9,'label': 'MAIN', 'title': 'MAIN'}],
  edges: [
    { from: 3, to: 1 },
    { from: 3, to: 7 },
    { from: 5, to: 3 },
    { from: 6, to: 4 },
    { from: 4, to: 2  },
      { from: 4, to: 8 },
      { from: 9, to: 3 },
      { from: 9, to: 4},
      { from: 9, to: 5 },
      { from: 9, to: 6}
  ]
};



const options = {
  layout: {
    improvedLayout: true
  },
  interaction:{
    hover:true,
    tooltipDelay:"10ms"
  },
  edges: {
    color: "#FFFFFF"
  },
  nodes:{
    shape:"circle"
  },
  height: "500px"
};


const events = {
  select: function(event) {
    var { nodes, edges } = event;
  }
};



const Index = (props) => {
  const [activeNav, setActiveNav] = useState(1);
  const [chartExample1Data, setChartExample1Data] = useState("data1");

  if (window.Chart) {
    parseOptions(Chart, chartOptions());
  }

  const toggleNavs = (e, index) => {
    e.preventDefault();
    setActiveNav(index);
    setChartExample1Data("data" + index);
  };








//   <Container className="mt--7" fluid>

//   <Card className="bg-gradient-default shadow">
//     <CardHeader className="bg-transparent">
//       <Row className="align-items-center">
//         <div className="col">
//           <h6 className="text-uppercase text-light ls-1 mb-1">
//             Overview
//           </h6>
//           <h2 className="text-white mb-0">User Details Graph </h2>
//         </div>
        
//       </Row>
//     </CardHeader>

//     <Graph
// graph={graph}
// options={options}
// events={events}
// getNetwork={network => {
// //  if you want access to vis.js network api you can set the state in a parent component using this property
// }}
// />

  return (
    <>
      <Header />
      <Container className="mt--7" fluid>
      <Row>
          <Col className="mb-5 mb-xl-0" xl="8">
            <Card className="bg-gradient-default shadow">
              
            <CardHeader className="bg-transparent">
              <Row className="align-items-center">
              <div className="col">
              <h6 className="text-uppercase text-light ls-1 mb-1">
               Overview  </h6>
         <h2 className="text-white mb-0">User Details Graph </h2>
        </div>
        
      </Row>
    </CardHeader>

  <Graph graph={graph} options={options} events={events} getNetwork={network => { //  if you want access to vis.js network api you can set the state in a parent component using this property
  }} />   
              
            </Card>
          </Col>
          <Col xl="4">
            <Card className="shadow">
              <CardHeader className="bg-transparent">
                <Row className="align-items-center">
                  <div className="col">
                    <h6 className="text-uppercase text-muted ls-1 mb-1">
                      Prevous
                    </h6>
                    <h2 className="mb-0">Breakdown by Years</h2>
                  </div>
                </Row>
              </CardHeader>
              <CardBody>
                {/* Chart */}
                <div className="chart">
                  <Bar
                    data={chartExample3.data}
                    options={chartExample3.options}
                  />
                </div>
              </CardBody>
            </Card>
          </Col>
      {/* Page content */}
     
              {/* Chart */}
              {/* <CardBody>
                
                <div className="chart">
                  <Line
                    data={chartExample1[chartExample1Data]}
                    options={chartExample1.options}
                    getDatasetAtEvent={(e) => console.log(e)}
                  />
                </div>
              </CardBody> */}
            {/* </Card> */}
            
          </Row>
        <Row className="mt-5">
          <Col className="mb-5 mb-xl-0" xl="8">
            <Card className="shadow">
              <CardHeader className="border-0">
                <Row className="align-items-center">
                  <div className="col">
                    <h3 className="mb-0">Type of Breakdowns</h3>
                  </div>
                
                </Row>
              </CardHeader>
              <Table className="align-items-center table-flush" responsive>
                <thead className="thead-light">
                  <tr>
                    <th scope="col">Type</th>
                    <th scope="col">Count</th>
                    <th scope="col">Average Response Time</th>
                    <th scope="col">MtM</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <th scope="row">Battery Jump Start </th>
                    <td>4,569</td>
                    <td>340</td>
                    <td>
                      <i className="fas fa-arrow-up text-success mr-3" /> 46,53%
                    </td>
                  </tr>
                  <tr>
                    <th scope="row">Extrication</th>
                    <td>3,985</td>
                    <td>319</td>
                    <td>
                      <i className="fas fa-arrow-down text-warning mr-3" />{" "}
                      46,53%
                    </td>
                  </tr>
                  <tr>
                    <th scope="row">Flat Tire Service</th>
                    <td>3,513</td>
                    <td>294</td>
                    <td>
                      <i className="fas fa-arrow-down text-warning mr-3" />{" "}
                      36,49%
                    </td>
                  </tr>
                  <tr>
                    <th scope="row">Fuel Delivery  </th>
                    <td>2,050</td>
                    <td>147</td>
                    <td>
                      <i className="fas fa-arrow-up text-success mr-3" /> 50,87%
                    </td>
                  </tr>
                  <tr>
                    <th scope="row">Lockout</th>
                    <td>1,795</td>
                    <td>190</td>
                    <td>
                      <i className="fas fa-arrow-down text-danger mr-3" />{" "}
                      46,53%
                    </td>
                  </tr>
                  <tr>
                    <th scope="row">Locksmith</th>
                    <td>1,795</td>
                    <td>190</td>
                    <td>
                      <i className="fas fa-arrow-down text-danger mr-3" />{" "}
                      46,53%
                    </td>
                  </tr>

                  <tr>
                    <th scope="row">Other</th>
                    <td>1,795</td>
                    <td>190</td>
                    <td>
                      <i className="fas fa-arrow-down text-danger mr-3" />{" "}
                      46,53%
                    </td>
                  </tr>
                  <tr>
                    <th scope="row">Possible OTG Opportunity</th>
                    <td>1,795</td>
                    <td>190</td>
                    <td>
                      <i className="fas fa-arrow-down text-danger mr-3" />{" "}
                      46,53%
                    </td>
                  </tr>
                  <tr>
                    <th scope="row">Tow</th>
                    <td>1,795</td>
                    <td>190</td>
                    <td>
                      <i className="fas fa-arrow-down text-danger mr-3" />{" "}
                      46,53%
                    </td>
                  </tr>
                </tbody>
              </Table>
            </Card>
          </Col>
          <Col xl="4">
            <Card className="shadow">
              <CardHeader className="border-0">
                <Row className="align-items-center">
                  <div className="col">
                    <h3 className="mb-0">Truck Type</h3>
                  </div>
                 
                </Row>
              </CardHeader>
              <Table className="align-items-center table-flush" responsive>
                <thead className="thead-light">
                  <tr>
                    <th scope="col">Truck Type</th>
                    <th scope="col">Count</th>
                    <th scope="col">Avg Time</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <th scope="row">Flat Bed </th>
                    <td>1,480</td>
                    <td>00:32:00</td>
                    
                  </tr>
                  <tr>
                    <th scope="row">Light Service</th>
                    <td>5,480</td>
                    <td>00:32:00</td>
                    
                  </tr>
                  <tr>
                    <th scope="row">Locksmith  </th>
                    <td>4,807</td>
                    <td>00:32:00</td>
                    
                  </tr>
                  <tr>
                    <th scope="row">Sling</th>
                    <td>3,678</td>
                    <td>00:32:00</td>

                  </tr>
                  <tr>
                    <th scope="row">Structure Truck  </th>
                    <td>2,645</td>
                    <td>00:32:00</td>
                    
                  </tr>
                  <tr>
                    <th scope="row">Wheel Lift           </th>
                    <td>2,645</td>
                    <td>00:32:00</td>
                    
                  </tr>
                </tbody>
              </Table>
            </Card>
          </Col>
        </Row>
      </Container>
    </>
  );
};

export default Index;
